#!/bin/bash

set +e
gtester build_dir/dexela_tests --keep-going -o=testresults.xml
echo "xsltproc -o junit-testresults.xml tools/gtester.xsl testresults.xml"
xsltproc -o junit-testresults.xml tools/gtester.xsl testresults.xml
exit 0
