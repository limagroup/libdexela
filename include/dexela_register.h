#include <glib.h>

#ifndef DEXELA_REGISTER_H
#define DEXELA_REGISTER_H

gint getBinningModeNumberFor(gint horizontal, gint vertical);

guint getBinningOrValueFor(gint horizontal, gint vertical);

gint getLowerBitsFor(gint number);

gint getHigherBitsFor(gint number);

gint getCombinedRegisters(gint higherBits, gint lowerBits);

#endif /* DEXELA_REGISTER_H */