#ifndef DEXELA_LOWLEVEL_H
#define DEXELA_LOWLEVEL_H


#include <glib.h>

G_BEGIN_DECLS

extern const gint RESERVED;
extern const gint SINGLE_UNIT;
extern const gint MAX_ANSWER_LENGTH;

void handlePXD(const gchar* name, gint returnValue);

/**
 * @return register value
 */
gint readSensorRegisterValue(gint sensorAddress, gint registerAddress);

/**
 * @return 0 on success, non-zero otherwise
 */
gint writeSensorRegisterValue(gint sensorAddress, gint registerAddress, gint value); 

/**
 * @return register value
 */
gint readRegisterValue(gint registerAddress);

/**
 * @return 0 on success, non-zero otherwise
 */
gint writeRegisterValue(gint registerAddress, gint value); 

/**
 * @return 0 on success, non-zero otherwise
 */
gint command(gchar* name); 

G_END_DECLS

#endif /* DEXELA_LOWLEVEL_H */
