#ifndef DEXELA_API_H
#define DEXELA_API_H

#include <glib.h>
#include "config.h"

G_BEGIN_DECLS

typedef enum {
    NORMAL,
    SEQUENCE,
    FRAME_RATE,
    EXPOSURE_UNKNOWN,
} ExposureMode;

typedef enum {
    LOW,
    HIGH,
} Gain;

typedef enum {
    SOFTWARE,
    EDGE,
    DURATION,
    TRIGGER_UNKNOWN,
} TriggerMode;

/**
 * @brief dexela_open_detector
 * @param formatFile
 * @return TRUE on success, FALSE otherwise
 */
gboolean dexela_open_detector(const gchar* formatFile);

/**
 * @brief dexela_close_detector
 * @return TRUE on success, FALSE otherwise
 */
gboolean dexela_close_detector();

gint dexela_init_serial_connection();

gint dexela_power_on_sensor();

gint dexela_power_off_sensor();

gint dexela_link_wakeup();

gchar* dexela_get_model();

gchar* dexela_get_serial_number();

gchar* dexela_get_firmware_version();

gint dexela_get_bit_depth();

gint dexela_get_width();

gint dexela_get_height();

gint dexela_get_exposure_time_micros();

gint dexela_set_exposure_time_micros(gint micros);

ExposureMode dexela_get_exposure_mode();

gint dexela_set_exposure_mode(ExposureMode mode, gint numberOfExposures, gint gapTime);

Gain dexela_get_gain();

gint dexela_set_gain(Gain gain);

TriggerMode dexela_get_trigger_mode();

gint dexela_set_trigger_mode(TriggerMode mode);

gint dexela_get_binning_mode_horizontal();

gint dexela_get_binning_mode_vertical();

gint dexela_set_binning_mode(gint horizontal, gint vertical);

gint dexela_get_control_register();

gint dexela_set_control_register(gint value);

/**
 * Currently placeholder API
 */
gint dexela_start_acquisition();

/**
 * @returns a pointer to the image buffer
 */
guchar* dexela_grab();

/**
 * Currently placeholder API
 */
gint dexela_stop_acquisition();

/**
 * Intended for testing purposes only. Will be removed in a future version of the
 * library.
 */
gint dexela_grab_and_save();

G_END_DECLS

#endif /* DEXELA_API_H */
