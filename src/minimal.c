#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include "xclib/xcliball.h"
#include "config.h"

static const int BUFFER_NUMBER = 1;
static const int RESERVED = 0;
static const int SINGLE_UNIT = 0x1;
static const int MAX_ANSWER_LENGTH = 100;
static unsigned char *image;

static void handlePXD(const char* name, int returnValue)
{
    if (returnValue >= 0) {
//      printf("Ok %s.\n", name);
        return;
    }
    printf("Error %s: %d\n", name, returnValue);
    printf("Message: %s\n", pxd_mesgErrorCode(returnValue));
}

int getCombinedRegisters(int higherBits, int lowerBits)
{
    return (higherBits << 16) + lowerBits;
}

int getLowerBitsFor(int number)
{
    return number & 0xFFFF;
}

int getHigherBitsFor(int number)
{
    return number >> 16;
}

/**
 * Read commands take the form:
 * R<sensor address><register address><CR>
 * 
 * Write commands take the form:
 * W<sensor address><register address><5 digit 0 padded value><CR>
 * 
 * Actions take the form:
 * WP<1 or 0><CR>
 */
void sendRequest(char* request)
{
    char requestBuffer[MAX_ANSWER_LENGTH];
    snprintf(requestBuffer, MAX_ANSWER_LENGTH, "%s\r", request);
    handlePXD("serial write", pxd_serialWrite(SINGLE_UNIT, RESERVED, requestBuffer, strlen(requestBuffer)));
} 
/**
 * Client must provide an allocated buffer with size MAX_ANSWER_LENGTH.
 * For convenience a pointer to the buffer is returned in case of success,
 * else NULL is returned.
 */
char* readAnswer(char* answer)
{
    int dataCount = 0;
    char buffer[255];
    while (1) {
        int read = pxd_serialRead(SINGLE_UNIT, RESERVED, &buffer[dataCount], 255 - dataCount);
        dataCount += read;
        if (dataCount > MAX_ANSWER_LENGTH) {
            printf("Cannot handle answer, because it is too long: %d, max: %d\n", dataCount, MAX_ANSWER_LENGTH);
            return NULL;
        }
        snprintf(answer, dataCount, "%s", buffer);
        handlePXD("serial read", read);
        if (read == 0) {
//          printf("Read nothing...\n");
            usleep(10000);
            continue;
        }
//      printf("Read result: %s, count: %d\n", answer, dataCount);
        if (dataCount > 0) {
            break;
        }
    }
    return answer;
}

int readSensorRegisterValue(int sensorAddress, int registerAddress)
{
    char request[MAX_ANSWER_LENGTH];
    char answer[MAX_ANSWER_LENGTH];
    
    snprintf(request, MAX_ANSWER_LENGTH, "R%d%03d", sensorAddress, registerAddress);
    printf("Read request: %s\n", request);
    sendRequest(request);
    readAnswer(answer);
    printf("Read answer: %s\n", answer);
    return strtol(answer + 1, NULL, 10);
}

int readRegisterValue(int registerAddress)
{
    return readSensorRegisterValue(1, registerAddress);
}

/**
 * @return 0 on success, non-zero otherwise
 */
int writeSensorRegisterValue(int sensorAddress, int registerAddress, int value)
{
    char request[MAX_ANSWER_LENGTH];
    char answer[MAX_ANSWER_LENGTH];
    
    snprintf(request, MAX_ANSWER_LENGTH, "W%d%03d%05d", sensorAddress, registerAddress, value);
    printf("Write request: %s\n", request);
    sendRequest(request);
    readAnswer(answer);
    printf("Write answer: %s\n", answer);
    return strcmp(answer, "X");
}

/**
 * @return 0 on success, non-zero otherwise
 */
int writeRegisterValue(int registerAddress, int value)
{
    return writeSensorRegisterValue(0, registerAddress, value);
}

int dexela_get_control_register()
{
    return readSensorRegisterValue(1, 0);
}

int dexela_set_control_register(int value)
{
    return writeSensorRegisterValue(1, 0, value);
}

/**
 * Dexela has 14 bits per pixel,
 * but looks to the frame grabber like 8bit with double horizontal resolution
 */
int dexela_get_bit_depth()
{
    return 14;
}

/**
 * Dexela has 14 bits per pixel,
 * but looks to the frame grabber like 8bit with double horizontal resolution
 */
int dexela_get_width()
{
    return pxd_imageXdim() / 2;
}

int dexela_get_height()
{
    return pxd_imageYdim();
}

static void writePGM(const char* filename, unsigned char* buffer)
{
    FILE* out = fopen(filename, "wb");
    char* header = "P5\n1536 864\n65535\n";
    fwrite(header, sizeof(char), strlen(header), out);
    fwrite(buffer, sizeof(char), 3072 * 864, out);
    fclose(out);
}

int dexela_open_detector(char* formatFile)
{
    return pxd_PIXCIopen("", NULL, formatFile);
}

int dexela_close_detector()
{
    free(image);
    return pxd_PIXCIclose();
}

int dexela_init_serial_connection()
{
    return pxd_serialConfigure(SINGLE_UNIT, RESERVED, 115200, 8, 0, 1, RESERVED, RESERVED, RESERVED);
}

int dexela_set_exposure_mode()
{
    // NORMAL
    int controlRegister = dexela_get_control_register();
    // set single frame exposure: bit 3 ON
    controlRegister |= 0x0008;
    // clear exposure mode bits 12 and 13
    controlRegister &= 0xDFFF;
    controlRegister &= 0xEFFF;
    return dexela_set_control_register(controlRegister);
}

int dexela_get_exposure_time_micros()
{
    int lowerBits = readRegisterValue(11);
    int higherBits = readRegisterValue(12);
    return getCombinedRegisters(higherBits, lowerBits) * 10;
}

int dexela_set_exposure_time_micros(int micros)
{
    int leastResult = writeRegisterValue(11, getLowerBitsFor(micros / 10));
    int mostResult = writeRegisterValue(12, getHigherBitsFor(micros / 10));
    return leastResult || mostResult;
}

unsigned char* dexela_grab()
{
    // wait up to 15s for the next frame
    handlePXD("capturing", pxd_doSnap(SINGLE_UNIT, BUFFER_NUMBER, 15000));
    int bytes = pxd_imageXdim() * pxd_imageYdim() * sizeof(unsigned char);
    if (image == NULL) {
        printf("Allocating image buffer of size: %d\n", bytes);
        image = malloc(bytes);
    }
    pxd_readuchar(SINGLE_UNIT, BUFFER_NUMBER, 0, 0, -1, -1, image, bytes, "Grey");
    // descramble image data
    for (int i = 0; i < bytes; i += 2) {
        char temp = image[i];
        image[i] = image[i + 1];
        image[i + 1] = temp;
    }
    return image;
}

int main(int argc, char** argv)
{
    char fmtFileName[100];
    snprintf(fmtFileName, 100, "share/%s", DEFAULT_FMT_FILE_NAME);
    int result = dexela_open_detector(fmtFileName);
    handlePXD("open", result);
    if (result != 0) {
        printf("Open failed. Exiting...\n");
        return -1;
    }
    handlePXD("serial configure", dexela_init_serial_connection());
    printf("Set exposure time to 5s: %d\n", dexela_set_exposure_time_micros(5));
    printf("Exposure time micros: %d\n", dexela_get_exposure_time_micros());
    printf("Setting exposure mode to NORMAL: %d\n", dexela_set_exposure_mode());
    printf("Image widtdh:%d\n", dexela_get_width());
    printf("Image height:%d\n", dexela_get_height());
    printf("Image bits:%d\n", dexela_get_bit_depth());
    printf("Control register test image: %d\n", dexela_get_control_register() & 1);
    printf("Set Control register test image: %d\n", dexela_set_control_register(dexela_get_control_register() & ~1));
    printf("Set software trigger mode...\n");
    int clearedTrigger = dexela_get_control_register() & 0x071F;
    dexela_set_control_register(clearedTrigger);

    struct timeval tvBegin;
    struct timeval tvEnd;
    char ownFileName[50];
    for (int i = 0; i < 10; i++) {
        snprintf(ownFileName, 50, "own%d.pgm", i);
        gettimeofday(&tvBegin, NULL);
        unsigned char *buffer = dexela_grab();
        gettimeofday(&tvEnd, NULL);
        printf("Grabbed %p\n", buffer);
        printf("Snap took: %ld millis\n", (tvEnd.tv_sec - tvBegin.tv_sec) * 1000 + (tvEnd.tv_usec - tvBegin.tv_usec) / 1000);
        writePGM(ownFileName, buffer);
    }
    handlePXD("close", dexela_close_detector());
    return 0;
}
