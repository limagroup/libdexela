#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <unistd.h>
#include <stdlib.h>
#include "xclib/xcliball.h"

#include "dexela_lowlevel.h"

const gint RESERVED = 0;
const gint SINGLE_UNIT = 0x1;
const gint MAX_ANSWER_LENGTH = 100;
G_LOCK_DEFINE_STATIC (serial);

void handlePXD(const gchar* name, gint returnValue)
{
    if (returnValue >= 0) {
        return;
    }
    g_critical("Error %s: %d", name, returnValue);
    g_critical("Message: %s", pxd_mesgErrorCode(returnValue));
    pxd_mesgFault(returnValue);
}

/**
 * Read commands take the form:
 * R<sensor address><register address><carriage return>
 * 
 * Write commands take the form:
 * W<sensor address><register address><5 digit 0 padded value><carriage return>
 * 
 * Actions take the form:
 * WP<1 or 0><carriage return>
 */
static void sendRequest(gchar* request)
{
    gchar requestBuffer[MAX_ANSWER_LENGTH];
    g_snprintf(requestBuffer, MAX_ANSWER_LENGTH, "%s\r", request);
    handlePXD("serial write", pxd_serialWrite(SINGLE_UNIT, RESERVED, requestBuffer, strlen(requestBuffer)));
} 
/**
 * Client must provide an allocated buffer with size MAX_ANSWER_LENGTH.
 * For convenience a pointer to the buffer is returned in case of success,
 * else NULL is returned.
 */
static gchar* readAnswer(gchar* answer)
{
    gint dataCount = 0;
    gchar buffer[MAX_ANSWER_LENGTH];
    while (1) {
        gint read = pxd_serialRead(SINGLE_UNIT, RESERVED, &buffer[dataCount], MAX_ANSWER_LENGTH - dataCount);
        dataCount += read;
        if (dataCount > MAX_ANSWER_LENGTH - 1) {
            g_critical("Cannot handle answer, because it is too long: %d, max: %d\n", dataCount, MAX_ANSWER_LENGTH);
            return NULL;
        }
        handlePXD("serial read", read);
        if (read == 0 || dataCount == 0) {
//			printf("Read nothing...\n");
            usleep(10000);
            continue;
        }
        buffer[dataCount] = '\0';
//		printf("Read result: %s, count: %d\n", answer, dataCount);
        // the only valid answers are "X", "E" or "S<5 digits>". We stop reading unknown
        // or too long answers
        if (g_strcmp0(buffer, "X") || g_strcmp0(buffer, "E")) {
            break;
        }
        if (!g_str_has_prefix(buffer, "S")) {
            break;
        }
        if (dataCount >= 6) {
            break;
        }
    }
    g_snprintf(answer, dataCount, "%s", buffer);
    return answer;
}

gint readSensorRegisterValue(gint sensorAddress, gint registerAddress)
{
    gchar request[MAX_ANSWER_LENGTH];
    gchar answer[MAX_ANSWER_LENGTH];

    g_snprintf(request, MAX_ANSWER_LENGTH, "R%d%03d", sensorAddress, registerAddress);
    G_LOCK (serial);
    g_debug("Read request: %s", request);
    sendRequest(request);
    readAnswer(answer);
    g_debug("Read answer: %s", answer);
    G_UNLOCK (serial);
    return strtol(answer + 1, NULL, 10);
}

gint readRegisterValue(gint registerAddress)
{
    return readSensorRegisterValue(1, registerAddress);
}

/**
 * @return 0 on success, non-zero otherwise
 */
gint writeSensorRegisterValue(gint sensorAddress, gint registerAddress, gint value)
{
    gchar request[MAX_ANSWER_LENGTH];
    gchar answer[MAX_ANSWER_LENGTH];
    
    g_snprintf(request, MAX_ANSWER_LENGTH, "W%d%03d%05d", sensorAddress, registerAddress, value);
    G_LOCK (serial);
    g_debug("Write request: %s", request);
    sendRequest(request);
    readAnswer(answer);
    g_debug("Write answer: %s", answer);
    G_UNLOCK (serial);
    return g_strcmp0(answer, "X");
}

/**
 * @return 0 on success, non-zero otherwise
 */
gint writeRegisterValue(gint registerAddress, gint value)
{
    return writeSensorRegisterValue(0, registerAddress, value);
}

/**
 * @return 0 on success, non-zero otherwise
 */
gint command(gchar* name)
{
    gchar answer[MAX_ANSWER_LENGTH];
    G_LOCK (serial);
    sendRequest(name);
    readAnswer(answer);
    G_UNLOCK (serial);
    return g_strcmp0(answer, "X");
}
