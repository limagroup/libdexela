#include <glib.h>

gint getCombinedRegisters(gint higherBits, gint lowerBits)
{
	return (higherBits << 16) + lowerBits;
}

gint getLowerBitsFor(gint number)
{
	return number & 0xFFFF;
}

gint getHigherBitsFor(gint number)
{
	return (number >> 16) & 0xFFFF;
}


gint getBinningModeNumberFor(gint horizontal, gint vertical)
{
	return (horizontal << 3) + vertical;
}

guint getBinningOrValueFor(gint horizontal, gint vertical)
{
	static GHashTable* binningModeOrValue = NULL;
	if (!binningModeOrValue) {
		binningModeOrValue = g_hash_table_new(g_direct_hash, g_direct_equal);
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(1, 1)), GUINT_TO_POINTER(0x0000u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(1, 2)), GUINT_TO_POINTER(0x0000u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(1, 4)), GUINT_TO_POINTER(0x0040u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(2, 1)), GUINT_TO_POINTER(0x0080u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(2, 2)), GUINT_TO_POINTER(0x0080u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(2, 4)), GUINT_TO_POINTER(0x00C0u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(4, 1)), GUINT_TO_POINTER(0x0100u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(4, 2)), GUINT_TO_POINTER(0x0100u));
		g_hash_table_insert(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(4, 4)), GUINT_TO_POINTER(0x0140u));
	}
	return GPOINTER_TO_UINT(g_hash_table_lookup(binningModeOrValue, GINT_TO_POINTER(getBinningModeNumberFor(horizontal, vertical))));
}
