#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include "dexela_lowlevel.h"
#include "dexela_register.h"
#include "dexela_api.h"

#include "xclib/xcliball.h"

static guchar *rawImage;
static guchar *resultImage;
static gint imageByteCount;
static const gint BUFFER_NUMBER = 1;
static const gint AOI_EXTREME_LEFT = 0;
static const gint AOI_EXTREME_TOP = 0;
static const gint AOI_EXTREME_RIGHT = -1;
static const gint AOI_EXTREME_BOTTOM = -1;
static gboolean running = FALSE;
static sigset_t signalMask;
static TriggerMode activeTriggerMode;
static int MODEL = 1207;  // think of initializing using dexela_get_model()
static gint currentHorizontalBinning;
static gint currentVerticalBinning;

static guchar* toggleEndianess(guchar* buffer, gint bufferSize)
{
    for (gint i = 0; i < bufferSize; i += 2) {
        gchar temp = buffer[i];
        buffer[i] = buffer[i + 1];
        buffer[i + 1] = temp;
    }
    return buffer;
}

gboolean dexela_open_detector(const gchar* formatFile)
{
    g_debug("open detector");
    gint result = pxd_PIXCIopen("", NULL, formatFile);
    if (result != 0) {
        return FALSE;
    }
    imageByteCount = pxd_imageXdim() * pxd_imageYdim() * sizeof(guchar);
    g_debug("Allocating image buffer of size: %d", imageByteCount);
    if (rawImage == NULL) {
        rawImage = g_malloc(imageByteCount);
    }
    if (resultImage == NULL) {
        resultImage = g_malloc(imageByteCount);
    }
    return TRUE;
}

gboolean dexela_close_detector()
{
    g_free(rawImage);
    rawImage = NULL;
    g_free(resultImage);
    resultImage = NULL;
    gint result = pxd_PIXCIclose();
    if (result != 0) {
        return FALSE;
    }
    return TRUE;
}

gint dexela_init_serial_connection()
{
    gint result = pxd_serialConfigure(SINGLE_UNIT, RESERVED, 115200, 8, 0, 1, RESERVED, RESERVED, RESERVED);
    // SCHNEIDE-450 initialise binnings here, so that we do not have to ask every frame.
    // This should increase stability even though serial communication may still fail.
    // This place is chosen because this function is the last one that always has to
    // be called
    currentHorizontalBinning = dexela_get_binning_mode_horizontal();
    currentVerticalBinning = dexela_get_binning_mode_vertical();
    return result;
}

gint dexela_power_on_sensor()
{
    return command("WP1");
}

gint dexela_power_off_sensor()
{
    return command("WP0");
}

gint dexela_link_wakeup()
{
    return command("WC1");
}

gchar* dexela_get_model()
{
    return g_strdup_printf("%d", readRegisterValue(124));
}


gchar* dexela_get_serial_number()
{
    return g_strdup_printf("%d", readRegisterValue(125));
}

gchar* dexela_get_firmware_version()
{
    return g_strdup_printf("%d", readRegisterValue(126));
}

/**
 * Dexela has 14 bits per pixel,
 */
gint dexela_get_bit_depth()
{
    return 14;
}

/**
 * Dexela has 14 bits per pixel,
 * but looks to the frame grabber like 8bit with double horizontal resolution
 */
gint dexela_get_width()
{
    return (pxd_imageXdim() / currentHorizontalBinning) / 2;
}

gint dexela_get_height()
{
    return pxd_imageYdim() / currentVerticalBinning;
}

/**
 * TODO: check for correctness
 */
gint dexela_get_exposure_time_micros()
{
    gint lowerBits = readRegisterValue(11);
    gint higherBits = readRegisterValue(12);
    return getCombinedRegisters(higherBits, lowerBits) * 10;
}

/**
 * TODO: check for correctness
 */
gint dexela_set_exposure_time_micros(gint micros)
{
    gint leastResult = writeRegisterValue(11, getLowerBitsFor(micros / 10));
    gint mostResult = writeRegisterValue(12, getHigherBitsFor(micros / 10));
    return leastResult || mostResult;
}

ExposureMode dexela_get_exposure_mode()
{
    gint controlRegister = dexela_get_control_register();
    g_debug("Exposure single/multi bit: %d", controlRegister & 0x0008);
    g_debug("Exposure bit 12: %d", controlRegister & 0x1000);
    g_debug("Exposure bit 13: %d", controlRegister & 0x2000);
    gboolean singleExposure =  (controlRegister & 0x0008) != 0;
    gboolean bit13 =  (controlRegister & 0x2000) != 0;
    gboolean bit12 =  (controlRegister & 0x1000) != 0;
    if (singleExposure) {
        return NORMAL;
    }
    if (!bit13 && !bit12) {
        return SEQUENCE;
    }
    if (bit13 && !bit12) {
        return FRAME_RATE;
    }
    return EXPOSURE_UNKNOWN;
}

/**
 * @param numberOfExposures ignored for NORMAL mode
 * @param gapTime ignored for NORMAL and SEQUENTIAL mode
 * @return 0 on success, non-zero otherwise
 */
gint dexela_set_exposure_mode(ExposureMode mode, gint numberOfExposures, gint gapTime)
{
    if (mode == NORMAL) {
        gint controlRegister = dexela_get_control_register();
        // set single frame exposure: bit 3 ON
        controlRegister |= 0x0008;
        // clear exposure mode bits 12 and 13
        controlRegister &= 0xDFFF;
        controlRegister &= 0xEFFF;
        return dexela_set_control_register(controlRegister);
    }
    if (mode == SEQUENCE) {
        gint controlRegister = dexela_get_control_register();
        // set to multi frame exposure: bit 3 OFF
        controlRegister &= 0xFFF7;
        // clear exposure mode bits 12 and 13
        controlRegister &= 0xDFFF;
        controlRegister &= 0xEFFF;
        return dexela_set_control_register(controlRegister) | writeRegisterValue(17, numberOfExposures);
    }
    if (mode == FRAME_RATE) {
        gint controlRegister = dexela_get_control_register();
        // set to multi frame exposure: bit 3 OFF
        controlRegister &= 0xFFF7;
        // set exposure mode bit 13 ON
        controlRegister |= 0x2000;
        // clear exposure mode bits 12
        controlRegister &= 0xEFFF;
        return dexela_set_control_register(controlRegister) | writeRegisterValue(17, numberOfExposures) | writeRegisterValue(18, gapTime);
    }
    return -1;
}

Gain dexela_get_gain()
{
    gint sensorSettings = readRegisterValue(3);
    if (sensorSettings & 0x0004) {
        return HIGH;
    }
    return LOW;
}

gint dexela_set_gain(Gain gain)
{
    gint sensorSettings = readRegisterValue(3);
    if (gain == LOW) {
        return writeRegisterValue(3, sensorSettings & 0xFFFB);
    }
    if (gain == HIGH) {
        return writeRegisterValue(3, sensorSettings | 0x0004);
    }
    return -1;
}

TriggerMode dexela_get_trigger_mode()
{
    gint triggerBits = dexela_get_control_register() & 0x00E0;
    if (triggerBits == 0) {
        return SOFTWARE;
    }
    if (triggerBits == 0x0060) {
        return EDGE;
    }
    if (triggerBits == 0x00A0) {
        return DURATION;
    }
    return TRIGGER_UNKNOWN;
}

gint dexela_set_trigger_mode(TriggerMode mode)
{
    gint clearedTrigger = dexela_get_control_register() & 0x071F;
    if (mode == SOFTWARE) {
        return dexela_set_control_register(clearedTrigger);
    }
    if (mode == EDGE) {
        return dexela_set_control_register(clearedTrigger | 0x0060);
    }
    if (mode == DURATION) {
        return dexela_set_control_register(clearedTrigger | 0x00A0);
    }
    return -1;
}

gint dexela_get_binning_mode_horizontal()
{
    currentHorizontalBinning = readRegisterValue(9);
    return currentHorizontalBinning;
}

gint dexela_get_binning_mode_vertical()
{
    currentVerticalBinning = readRegisterValue(10);
    return currentVerticalBinning;
}

gint dexela_set_binning_mode(gint horizontal, gint vertical)
{
    gint clearedCurrentBinLevel = readRegisterValue(3) & 65087;
    writeRegisterValue(9, horizontal);
    writeRegisterValue(10, vertical);
    gint orValue = getBinningOrValueFor(horizontal, vertical);
    writeRegisterValue(3, clearedCurrentBinLevel | orValue);
    // commit to sensor
    gint error = writeRegisterValue(5, 514);
    if (!error) {
        currentHorizontalBinning = horizontal;
        currentVerticalBinning = vertical;
    }
    return error;
}

gint dexela_get_control_register()
{
    return readSensorRegisterValue(1, 0);
}

gint dexela_set_control_register(gint value)
{
    return writeSensorRegisterValue(1, 0, value);
}

/**
 * Currently placeholder API
 */
gint dexela_start_acquisition()
{
    if (running) {
        return 0;
    }
    g_debug("Not running, starting signal handling...");
    running = TRUE;
    activeTriggerMode = dexela_get_trigger_mode();
    return 0;
}




/**
 * @brief New descambling function from Oscar (Dexela) from 10 Mar 2015
 *        Works only for symetric binning:
 *        (currentVerticalBinning == current HorizontalBinning)
 *
 *
 * @param input interlaced image data buffer
 * @param output deinterlaced image data buffer
 */
static void descramble(ushort* input, ushort* output)
{ 
    
    //length of a strip (i.e. read-out block) in the sensor
    int stripLength = 256;
    //width of image (same for sorted and unsorted)
    int width;
    //height of sorted image
    int imHeight;
    //number of strips per sensor
    int strips = 6;
    //total number of sensors in detector
    int numSensors;
    //width of a single sensor in current binning mode (always 2x2 for the 2321 detector)
    int sensorWidth;
    //height of a single (large) sensor in current binning mode (always 2x2 for the 2321 detector)
    int sensorHeight;
    
    //set initial values depending on detector model
    switch(MODEL) {
    case 1512:
        width = 1536;
        imHeight = 1944;
        numSensors = 1;
        sensorWidth = 1536;
        sensorHeight = 1944;
        break;
    case 1207:
        width = 1536;
        imHeight = 864;
        numSensors = 1;
        sensorWidth = 1536;
        sensorHeight = 864;
        break;
    case 2923:
        width = 3072;
        imHeight = 3888;
        numSensors = 4;
        sensorWidth = 1536;
        sensorHeight = 1944;
        break;
    }
    
    //adjust relevant values to correspond to current binning mode
    if (currentVerticalBinning == 2) {
        imHeight /= 2; 
        sensorHeight /= 2;
    } else if(currentVerticalBinning == 4) {
        imHeight /= 4;
        sensorHeight /= 4;
    }
    if (currentHorizontalBinning == 2) {
        width /= 2;
        stripLength /= 2;
        sensorWidth /= 2;
    } else if(currentHorizontalBinning == 4) {
        width /= 4;
        stripLength /= 4;
        sensorWidth /= 4;
    }

    //total size of image
    int imSize = width * imHeight;
    //iterator used to read-out pixels sequentially from unsorted image
    int counter = 0;
    //offset to location for pixel in the output image
    int offset = 0;
    //iterate through all the rows in a sensor
    for (int i = 0; i < sensorHeight; i++) {
        //interate through all the pixels in a strip
        for (int j = 0; j < stripLength; j++) {
            //iterate through all strips in a single sensor
            for (int k = 0; k < strips; k++) {
                //iterate through all sensors
                for (int l = 0; l < numSensors; l++) {
                    //read in the value from the input image
                    int value = input[counter];
                    switch (l) {
                    case 0:
                        //for the first sensor we just need to move to the right row and right strip
                        offset = i * width + k * stripLength + j; 
                        break;
                    case 1:
                        //for the second sensor we have to do the same plus offset by a full sensor width
                        offset = i * width + sensorWidth + k * stripLength + j;
                        break;
                    case 2:
                        //for the third sensor we fill in from the end of the image
                        offset = imSize - (i * width + k * stripLength + j + 1);
                        break;
                    case 3:
                        //for the fourth sensor we do the same and offset it by a full sensor width
                        offset= imSize-(i * width + sensorWidth + k * stripLength + j + 1);
                        break;
                    }
                    output[offset] = value;
                    counter++;
                }
            }
        }
    }
}

/**
 * Wait for frame creation and copy it into our buffer
 */
guchar* dexela_grab()
{
    sigaddset(&signalMask, SIGUSR1);
    sigprocmask(SIG_BLOCK, &signalMask, NULL);
    handlePXD("enable capture signalling", pxd_eventCapturedFieldCreate(SINGLE_UNIT, SIGUSR1, RESERVED));
    handlePXD("capturing", pxd_goSnap(SINGLE_UNIT, BUFFER_NUMBER));
    g_debug("grabbing in mode %d", activeTriggerMode);
    if (activeTriggerMode == SOFTWARE) {
        g_debug("this is softwaretriggering...");
        pxd_setCameraLinkCCOut(0x1, 0x1);
        pxd_setCameraLinkCCOut(0x1, 0x0);
    }
    int signal = sigwaitinfo(&signalMask, NULL);
    if (signal < 0) {
        g_critical(strerror(errno));
    }
    pxd_readuchar(SINGLE_UNIT, BUFFER_NUMBER, AOI_EXTREME_LEFT, AOI_EXTREME_TOP, AOI_EXTREME_RIGHT, AOI_EXTREME_BOTTOM, rawImage, imageByteCount, "Grey");
    descramble(rawImage, resultImage);
    return resultImage;
}

gint dexela_stop_acquisition()
{
    running = FALSE;
    handlePXD("close capture signalling", pxd_eventCapturedFieldClose(SINGLE_UNIT, SIGUSR1));
    return 0;
}

gint dexela_grab_and_save()
{
    pxd_doSnap(SINGLE_UNIT, 1, 0);
    // these values will be correct when using the right formatfile...
    gint xSize = pxd_imageXdim();
    gint ySize = pxd_imageYdim();
    gint pixels = xSize * ySize;
    guchar* buf = g_malloc(pixels * sizeof(guchar));
    pxd_readuchar(SINGLE_UNIT,         // select PIXCI(R) unit 1
                    BUFFER_NUMBER,     // select frame buffer 1
                    0, 0,        // upper left X, Y AOI of buffer
                    -1, -1,      // lower right X, Y AOI of buffer,
                                // -1 is an abbreviation for the maximum X or Y
                    buf,         // program buffer to be filled
                    pixels, // size of program buffer in short's
                    "Grey");     // color space to access
    g_print("x=%d, y=%d\n", xSize, ySize);
    handlePXD("save tiff", pxd_saveTiff(SINGLE_UNIT, "testimage.tiff", 1, 0, 0, -1, -1, RESERVED, RESERVED));
/*    for (int y = 0; y < ySize; y++) {
        for (int x = 0; x < xSize; x++) {
            g_print("%d ", buf[xSize * y + x]);
        }
        g_print("\n");
    }*/
    g_free(buf);
    return 0;
}
