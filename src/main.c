#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <sys/time.h>
#include <string.h>

#include "dexela_api.h"
#include "config.h"

static void writePGM(const gchar* filename, guchar* buffer, gint width, gint height)
{
    FILE* out = fopen(filename, "wb");
    gchar header[50];
    gint bytes = width * 2 * height;
    snprintf(header, 50, "P5\n%d %d\n65535\n", width, height);
    fwrite(header, sizeof(char), strlen(header), out);
    fwrite(buffer, sizeof(char), bytes, out);
    fclose(out);
}

static void writePGMCompleteBuffer(const gchar* filename, guchar* buffer, gint bufferSize)
{
    FILE* out = fopen(filename, "wb");
    gchar header[50];
    snprintf(header, 50, "P5\n1536 864\n65535\n");
    fwrite(header, sizeof(char), strlen(header), out);
    fwrite(buffer, sizeof(char), bufferSize, out);
    fclose(out);
}

static void toggleEndianess(guchar* buffer, gint bufferSize)
{
    for (gint i = 0; i < bufferSize; i += 2) {
        gchar temp = buffer[i];
        buffer[i] = buffer[i + 1];
        buffer[i + 1] = temp;
    }
}

static void writeTiff(const gchar* filename, guchar* buffer)
{
/*    cv::Mat gray(864, 3072 / 2, CV_16UC1, buffer);
    cv::imwrite(fileName, gray);*/
}

static void frame_grabbed(int signal)
{
    
}

gint main(gint argc, gchar** argv)
{
    char fmtFileName[100];
    snprintf(fmtFileName, 100, "share/%s", DEFAULT_FMT_FILE_NAME);
    if (!dexela_open_detector(fmtFileName)) {
        g_printf("Could not open detector\n");
        return -1;
    }
    handlePXD("serial configure", dexela_init_serial_connection());
    handlePXD("link wakeup", dexela_link_wakeup());
    gchar* model = dexela_get_model();
    g_printf("Model number: %s\n", model);
    g_free(model);
    gchar* serial = dexela_get_serial_number();
    g_printf("Serial number: %s\n", serial);
    g_free(serial);
    gchar* firmware = dexela_get_firmware_version();
    g_printf("Firmware number: %s\n", firmware);
    g_free(firmware);
    // exposure time 
    g_printf("Set exposure time to 1s: %d\n", dexela_set_exposure_time_micros(1 * 1000 * 1000));
    g_printf("Exposure time micros: %d\n", dexela_get_exposure_time_micros());
    // exposure mode round trip
//    g_printf("Exposure mode: %d\n", dexela_get_exposure_mode());
//    g_printf("Setting exposure mode SEQUENCE: %d\n", dexela_set_exposure_mode(SEQUENCE, 129, 0));
//    g_printf("Exposure mode: %d\n", dexela_get_exposure_mode());
//    g_printf("Setting exposure mode FRAME_RATE: %d\n", dexela_set_exposure_mode(FRAME_RATE, 129, 3));
//    g_printf("Exposure mode: %d\n", dexela_get_exposure_mode());
    g_printf("Setting exposure mode to NORMAL: %d\n", dexela_set_exposure_mode(NORMAL, 0, 0));
    g_printf("Exposure mode: %d\n", dexela_get_exposure_mode());
    // binning mode round trip
//    g_printf("Binning mode: %dx%d\n", dexela_get_binning_mode_horizontal(), dexela_get_binning_mode_vertical());
//    g_printf("Setting binning mode result: %d\n", dexela_set_binning_mode(1, 1));
    // gain round trip
//    g_printf("Gain: %d\n", dexela_get_gain());
//    g_printf("Gain set high full well : %d\n", dexela_set_gain(HIGH));
    g_printf("New gain: %d\n", dexela_get_gain());
    // trigger mode round trip
/*    TriggerMode tm = dexela_get_trigger_mode();
    g_printf("Trigger mode: %d\n", tm);
    g_printf("Set trigger mode duration: %d\n", dexela_set_trigger_mode(DURATION));
    g_printf("New trigger mode: %d\n", dexela_get_trigger_mode());
    g_printf("Reset original trigger mode: %d\n", dexela_set_trigger_mode(tm));
    g_printf("Control register value: %d\n", dexela_get_control_register());
*/
    g_printf("Image width:%d\n", dexela_get_width());
    g_printf("Image height:%d\n", dexela_get_height());
    g_printf("Image bits:%d\n", dexela_get_bit_depth());
    g_printf("Control register test image: %d\n", dexela_get_control_register() & 1);
    g_printf("Set Control register test image: %d\n", dexela_set_control_register(dexela_get_control_register() & ~1));
    g_printf("Control register test image: %d\n", dexela_get_control_register() & 1);
    g_printf("Set software trigger mode: %d\n", dexela_set_trigger_mode(SOFTWARE));

    g_printf("Set binning result: %d\n", dexela_set_binning_mode(1, 1));
    g_printf("New binning mode: %dx%d\n", dexela_get_binning_mode_horizontal(), dexela_get_binning_mode_vertical());

    struct timeval tvBegin;
    struct timeval tvEnd;
    dexela_start_acquisition();
    char ownFileName[50];
    for (int i = 0; i < 1; i++) {
        gettimeofday(&tvBegin, NULL);
        guchar *buffer = dexela_grab();
        gettimeofday(&tvEnd, NULL);
        g_debug("Snap took: %ld millis", (tvEnd.tv_sec - tvBegin.tv_sec) * 1000 + (tvEnd.tv_usec - tvBegin.tv_usec) / 1000);
        toggleEndianess(buffer, 1536 * 2 * 864);
        snprintf(ownFileName, 50, "own%d.pgm", i);
        writePGM(ownFileName, buffer, dexela_get_width(), dexela_get_height());
        snprintf(ownFileName, 50, "own-full%d.pgm", i);
        writePGMCompleteBuffer(ownFileName, buffer, 1536 * 2 * 864);
        snprintf(ownFileName, 50, "opencv%d.tiff", i);
        writeTiff(ownFileName, buffer);
    }
    dexela_stop_acquisition();
    if (!dexela_close_detector()) {
        g_printf("Could not close detector\n");
        return -1;
    }
    return 0;
}
