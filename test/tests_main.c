#include <glib.h>
#include "dexela_register.h"

void binningModeNumberTest(void)
{
	g_assert_cmpint(9, ==, getBinningModeNumberFor(1, 1));
	g_assert_cmpint(10, ==, getBinningModeNumberFor(1, 2));
	g_assert_cmpint(12, ==, getBinningModeNumberFor(1, 4));
	g_assert_cmpint(17, ==, getBinningModeNumberFor(2, 1));
	g_assert_cmpint(18, ==, getBinningModeNumberFor(2, 2));
	g_assert_cmpint(20, ==, getBinningModeNumberFor(2, 4));
	g_assert_cmpint(33, ==, getBinningModeNumberFor(4, 1));
	g_assert_cmpint(34, ==, getBinningModeNumberFor(4, 2));
	g_assert_cmpint(36, ==, getBinningModeNumberFor(4, 4));
}

void binningOrValueTest(void)
{
	g_assert_cmphex(0x0000u, ==, getBinningOrValueFor(1, 1));
	g_assert_cmphex(0x0000u, ==, getBinningOrValueFor(1, 2));
	g_assert_cmphex(0x0040u, ==, getBinningOrValueFor(1, 4));
	g_assert_cmphex(0x0080u, ==, getBinningOrValueFor(2, 1));
	g_assert_cmphex(0x0080u, ==, getBinningOrValueFor(2, 2));
	g_assert_cmphex(0x00C0u, ==, getBinningOrValueFor(2, 4));
	g_assert_cmphex(0x0100u, ==, getBinningOrValueFor(4, 1));
	g_assert_cmphex(0x0100u, ==, getBinningOrValueFor(4, 2));
	g_assert_cmphex(0x0140u, ==, getBinningOrValueFor(4, 4));
}

void lowerBitsTest(void)
{
	g_assert_cmpint(17, ==, getLowerBitsFor(17));
	g_assert_cmpint(65535, ==, getLowerBitsFor(65535));
	g_assert_cmpint(0, ==, getLowerBitsFor(65536));
	g_assert_cmpint(24464, ==, getLowerBitsFor(90000));
}

void higherBitsTest(void)
{
	g_assert_cmpint(0, ==, getHigherBitsFor(17));
	g_assert_cmpint(0, ==, getHigherBitsFor(65535));
	g_assert_cmpint(1, ==, getHigherBitsFor(65536));
	g_assert_cmpint(3, ==, getHigherBitsFor(200000));
}

void combineRegistersTest(void)
{
	g_assert_cmpint(17, ==, getCombinedRegisters(0, 17));
	g_assert_cmpint(65535, ==, getCombinedRegisters(0, 65535));
	g_assert_cmpint(65536, ==, getCombinedRegisters(1, 0));
	g_assert_cmpint(200000, ==, getCombinedRegisters(3, 3392));
}

int main(int argc, char** argv)
{
	g_test_init(&argc, &argv, NULL);
	g_test_add_func("/register/binningModeNumber", binningModeNumberTest);
	g_test_add_func("/register/binningOrValue", binningOrValueTest);
	g_test_add_func("/register/computeLowerBitsFor", lowerBitsTest);
	g_test_add_func("/register/computeHigherBitsFor", higherBitsTest);
	g_test_add_func("/register/combineRegisters", combineRegistersTest);
	return g_test_run();
}